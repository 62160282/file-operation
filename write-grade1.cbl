       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR. Nipitpon.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           05 STU-ID PIC X(8).
           05 MIDTERM-SCORE PIC 9(2)V9(2).
           05 FINAL-SCORE PIC 9(2)V9(2).
           05 PROJECT-SCORE PIC 9(2)V9(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE
           MOVE "62160060" TO STU-ID 
           MOVE "34.05" TO MIDTERM-SCORE 
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "10.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160061" TO STU-ID 
           MOVE "34.05" TO MIDTERM-SCORE 
           MOVE "28.25" TO FINAL-SCORE 
           MOVE "14.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160062" TO STU-ID 
           MOVE "44.05" TO MIDTERM-SCORE 
           MOVE "30.25" TO FINAL-SCORE 
           MOVE "15.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160063" TO STU-ID 
           MOVE "34.05" TO MIDTERM-SCORE 
           MOVE "22.25" TO FINAL-SCORE 
           MOVE "11.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160064" TO STU-ID 
           MOVE "30.05" TO MIDTERM-SCORE 
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "10.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 
           CLOSE SCORE-FILE 
           GOBACK
           .
